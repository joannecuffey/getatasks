//Using restcountries API - https://github.com/apilayer/restcountries
const apiKey = '6b9a218ef9d71d23'
const endpoint = 'https://restcountries.eu/rest/v2/name/' 

var form = document.getElementById('apiForm');

form.addEventListener('submit', function(e) {
    //Get search term from form input
    var searchText = document.querySelector('#searchTerm').value

    //Check search query not null
    if(searchText == "") { 
        document.getElementById('result').innerHTML = '<p class="error">You must enter a country</p>'
        }

    else{           
        const url = endpoint + searchText;

        //Use fetch to make request to API
        fetch(url)
            .then(handleErrors)
            .then(response => response.json()) 
            .then(function(data) {
                var json = data[0];                        
                generateResults(json)
            })
            .catch(function(error) { 
                console.log('Error retrieving data: ' + error);
            });
    }

    e.preventDefault();
});

//Check response status and log errors
function handleErrors(response) {
    if (response.status == 404) {
        document.getElementById('result').innerHTML = '<p class="error">Country not found</p>';
        throw Error(response.statusText);
    }
    else if (!response.ok){
        document.getElementById('result').innerHTML = '<p class="error">Error retrieving data from API</p>';
        throw Error(response.statusText);
    }
    return response;
}

//Get some data from JSON response and update HTML with results
function generateResults(json){
    var countryName = '<p>Native name: ' + json.nativeName + '</p>';
    var englishName = '<p>English name: ' + json.name + '</p>';
    var capital = '<p>Capital: ' + json.capital + '</p>';
    var population = '<p>Population: ' + json.population + '</p>';
    var countryFlag = '<div><p>Flag: <img src="' + json.flag + '" alt="flag" /></p>';
    let result = '<h3> Country Info </h3>' + countryName + englishName + capital + population + countryFlag;
    document.getElementById('result').innerHTML = result;

    return false;

}
